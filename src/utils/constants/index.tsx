import { TabProps } from "@/components/tabs/Tab"

export const dataTabs: TabProps[] = [
  {
    text: 'Data Total Sasaran',
    active: true
  },
  {
    text: 'Data Indikator IKP',
    active: false
  }
]

export const dataTotalSummaryImmunization = [
  {
    title: "Imunisasi Dasar Lengkap",
    value: "19.611.448",
    subtitle: " dari target cakupan",
    percent: 65.8,
  },
  {
    title: "Imunisasi Dasar Lengkap",
    value: "19.611.448",
    subtitle: " dari target cakupan",
    percent: 65.8,
  },
  {
    title: "Imunisasi Dasar Lengkap",
    value: "19.611.448",
    subtitle: " dari target cakupan",
    percent: 65.8,
  },
  {
    title: "Imunisasi Dasar Lengkap",
    value: "19.611.448",
    subtitle: " dari target cakupan",
    percent: 65.8,
  },
  {
    title: "Imunisasi Dasar Lengkap",
    value: "19.611.448",
    subtitle: " dari target cakupan",
    percent: 65.8,
  },
  {
    title: "Imunisasi Dasar Lengkap",
    value: "19.611.448",
    subtitle: " dari target cakupan",
    percent: 65.8,
  },
]

export const dataGraphRegionalRoutineImmunizationCoverageTrend = [
  {
    title: "Total Cakupan Imunisasi Rutin Lengkap Nasional Tahun 2023",
    value: "80",
    regional: ""
  },
  {
    title: "Cakupan Tertinggi Tahun 2023",
    value: "70",
    regional: "Jawa Tengah"
  },
  {
    title: "Cakupan Terendah Tahun 2023",
    value: "0",
    regional: "Papua Pegunungan"
  },
]

export const dataMonth = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
export const filterLocationOptions = [
  {
    label: "Lokasi Domisili Pasien",
    value: "Lokasi Domisili Pasien"
  },
  {
    label: "Lokasi Fasilitas Kesehatan",
    value: "Lokasi Fasilitas Kesehatan"
  }];