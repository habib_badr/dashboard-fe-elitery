export const sidebarNavigation = [
  {
    title: "Imunisasi Rutin Lengkap",
    path: "/dashboard/complete-routine-immunization"
  },
  {
    title: "Imunisasi Rutin Bayi",
    path: "/dashboard/routine-infant-immunization"
  },
  {
    title: "Imunisasi Rutin Baduta",
    path: "/dashboard/routine-baduta-immunization"
  },
  {
    title: "Imunisasi Kejar Bayi & Baduta",
    path: "/dashboard/baby-chase-immunization-baduta"
  },
  {
    title: "Kamus Data",
    path: "/dashboard/data-dictionary"
  },
  {
    title: "Metadata",
    path: "/dashboard/metadata"
  },
  {
    title: "Komentar",
    path: "/dashboard/comment"
  },
]